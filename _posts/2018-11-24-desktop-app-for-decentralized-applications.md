---
title: Desktop app for decentralized applications
layout: post
date: 2018-11-24 10:26:24 +0000

---
I think I figured out decentralizing applications including "Collabthings Parts".  

There's now **collabthings-api** node project is a bridge to scuttlebutt network and runs registered API services. This is basically the part that makes everything decentralized. I'll probably add a IPFS service here too.  

**Collabthings-app** project is a electron app that launches the api project and registers services there. Services will be then listed in the app and you I'm thinking you could launch a website through that so that website can use a API it wants locallly.  

**Collabthings-parts** is the old Collabthings app that uses the services to store bookmarks and implement search and stuff like that.  

I'm trying to create collabthings-app-docs application first where you could collab and write/share documents and changes would be shared through the scuttlebutt platform.  

It's looking pretty good and currently everything is pretty much running. Lists of values can be shared already. It either bookmarks for the design software or some simple document editing I'm going to implement next.

I'll probably need help with the app design. Currently it looks pretty terrible :D Also it could be re-branded easily as TZM app.